# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

appId    = "$APPID"
password = "PASSWORD_AZURE"
key_blob = "$KEY_BLOB"
